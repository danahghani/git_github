
![dan](profile.png)

# Dana Abdul Ghani
#### danahghani@gmail.com | +961-78950696
***
## EDUCATION 
* Beirut Arab University
Bachelor’s in Interior Designing and Architecture --- June 2018
Debbiyeh, Lebanon
* Sidon Official High School for Girls
Lebanese Baccalaureate (Economics and Sociology)  --- June 2015
Saida, Lebanon
* Al-Ofoq Al-Jadeed School
Lebanese Brevet --- June 2012
Brameye, Lebanon
***
## CERTIFICATES
* Interior Designing for Hotels/Motels from NABA University in Milano, Italy.
* ICDL for skills and competencies necessities from Norwegian center.
***
## Skills
#### Computer:

* Adept at Microsoft Office
* Adept at designing using 3ds max, revit, autocad, photshop
* Some programming skills (HTML, JS)

#### Languages:

* Arabic: Native Language
* English: First Language
* French: Intermediate (speaking, reading); basic (writing)

#### Other skills:

* Excellent in photography
* Excellent communication skills (psychology and ethics with clients)
* Good freehand drawing skills 
***
## WORK EXPERIENCE 
* Gallery Sales at Elite Designs --- October 2016 – February 2017 --- Mr. Saleh Tahan (Business owner | +9613342151)
Textiles, Materials, and Furniture Designing
Saida, South Lebanon

* Darazi Design
(+961-7-727421)
Lighting Mood Designing
Saida, South Lebanon
***
## HOBBIES AND INTERESTS
* Amused by Geometric Art and a skilled drawer in the same genre
* Highly interested in volunteering opportunities and a frequent volunteer at Amideast YES alumni events
* Photography
* Music
* Theatre
* Executions
* Painting in a minimal style
* Traveling 
